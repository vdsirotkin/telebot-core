package com.vdsirotkin.telegram;

import org.telegram.telegrambots.api.objects.Update;

/**
 * Created by vitaly on 17.08.16.
 */
public interface Handler {
    void handleMessage(Update update);

    Boolean isFinished();

    boolean isSupported(String text);
}
