package com.vdsirotkin.telegram;

import com.google.gson.Gson;
import io.github.benas.randombeans.EnhancedRandomBuilder;
import io.github.benas.randombeans.api.EnhancedRandom;

/**
 * Created by vitaly on 29.08.16.
 */
public class ConfigGenerator {

    public static String generateConfig(Class schema) {
        EnhancedRandom build = new EnhancedRandomBuilder().build();
        Object filledObject = build.nextObject(schema);
        return new Gson().toJson(filledObject);
    }

}
