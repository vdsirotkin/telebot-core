package com.vdsirotkin.telegram;

import org.apache.log4j.Logger;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.telegram.telegrambots.TelegramApiException;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

/**
 * Created by vitaly on 17.08.16.
 */
public abstract class TelegramBotClass extends TelegramLongPollingBot {

    private static Logger log = Logger.getLogger(TelegramBotClass.class);
    private Set<Handler> handlerSet;

    public Set<Handler> getHandlerSet() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        if (handlerSet == null) {
            handlerSet = new HashSet<>();
            Reflections ref = new Reflections("com.vdsirotkin.telegram");
            Set<Class<? extends Handler>> subTypesOf = ref.getSubTypesOf(Handler.class);
            for (Class<? extends Handler> aClass : subTypesOf) {
                handlerSet.add(aClass.getConstructor().newInstance());
            }
        }
        return handlerSet;
    }

    public abstract DefaultMessages getDefaultMessages();

    private HashMap<String, Handler> userHandlerMap = new HashMap<>();

    public void onUpdateReceived(Update update) {
        String chatId = update.getMessage().getChatId().toString();
        String text = update.getMessage().getText();

        try {
            if ("/cancel".equals(text)) {
                sendMessage(getDefaultMessages().getCancelCommand(chatId));
                userHandlerMap.remove(chatId);
            } else {
                Handler handler = userHandlerMap.get(chatId);
                if (handler != null) {
                    handler.handleMessage(update);
                    if (handler.isFinished()) {
                        userHandlerMap.remove(chatId);
                    }
                } else {
                    Stream<Handler> stream = getHandlerSet().stream();
                    Optional<Handler> first = stream.filter(handler1 -> handler1.isSupported(text)).findFirst();
                    if (first.isPresent()) {
                        handler = first.get().getClass().getConstructor().newInstance();
                        handler.handleMessage(update);
                        if (!handler.isFinished()) {
                            userHandlerMap.put(chatId, handler);
                        }
                    } else {
                        sendMessage(getDefaultMessages().getCantFindCommand(chatId));
                    }
                }
            }
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException | TelegramApiException e) {
            log.error(e, e);
        }
    }
}
