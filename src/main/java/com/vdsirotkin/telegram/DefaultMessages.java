package com.vdsirotkin.telegram;

import org.telegram.telegrambots.api.methods.send.SendMessage;

/**
 * Created by vitaly on 17.08.16.
 */
public interface DefaultMessages {

    SendMessage getCantFindCommand(String chatId);

    SendMessage getCancelCommand(String chatId);
}
